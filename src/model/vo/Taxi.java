package model.vo;

public class Taxi implements Comparable<Taxi> {

    /**
     * @return id - taxi_id
     */
	private String taxi_id;
	private String company;
	
	
	public Taxi(String id, String pCompany) {
		taxi_id = id;
		company = pCompany;
	}
	
    public String getTaxiId()
    {
        // TODO Auto-generated method stub
        return taxi_id;
    }

    /**
     * @return company
     */
    public String getCompany()
    {
        // TODO Auto-generated method stub
        return company;
    }

    @Override
    public int compareTo(Taxi o)
    {
        // TODO Auto-generated method stub
        return this.getTaxiId().compareTo(o.getTaxiId());
    }
}

package model.vo;

import java.util.Iterator;

import model.data_structures.IList;
import model.data_structures.Lista;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

    private String taxiId;
    private String compania;
    private Lista<Servicio> servicios;

    public TaxiConServicios(String taxiId, String compania){
        this.taxiId = taxiId;
        this.compania = compania;
        this.servicios = new Lista<Servicio>(); // inicializar la lista de servicios 
    }

    public String getTaxiId() {
        return taxiId;
    }

    public String getCompania() {
        return compania;
    }

    public IList<Servicio> getServicios()
    {
    	return servicios;
    }
    
    public int numeroServicios(){
        return servicios.getSize();
    }

    public void agregarServicio(Servicio servicio){
        servicios.add(servicio);
    }
    
    public int getPuntos() {
    	double puntos = 0;
    	int numServ = servicios.getSize();
    	double millas = 0;
    	double dinero = 0;
    	
    	Iterator<Servicio> servIte = servicios.iterator();
    	
    	while(servIte.hasNext()) {
    		Servicio servicio = servIte.next();
    		millas += servicio.getTrip_miles();
    		dinero += servicio.getTrip_total();
    	}
    	
    	puntos = numServ+millas+dinero;
    	
    	if(millas>0 && dinero>0) {
    		puntos += (millas*numServ)/dinero;
    	}
 
    	return (int)puntos;
    }

    @Override
    public int compareTo(TaxiConServicios o) {
    	return getTaxiId().compareTo(o.getTaxiId());
    }

    public void print(){
        System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
        for(Servicio s : servicios){
            System.out.println("\t"+s.getTrip_start_timestamp());
        }
        System.out.println("___________________________________");;
    }
}

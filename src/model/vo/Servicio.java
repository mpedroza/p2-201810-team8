package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Servicio implements Comparable<Servicio>{

	private String trip_id;
	private String taxi_id;
	private String company;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	private String trip_start_timestamp;
	private String trip_end_timestamp;
	private int pickup_community_area;
	private int dropoff_community_area;
	private String pickup_centroid_latitude;
	private String pickup_centroid_longitude;
	



	public String getTrip_id() {
		return trip_id;
	}

	public String getTaxi_id() {
		return taxi_id;
	}
	
	public String getCompany() {
		return company;
	}

	public int getTrip_seconds() {
		return trip_seconds;
	}

	public double getTrip_miles() {
		return trip_miles;
	}

	public double getTrip_total() {
		return trip_total;
	}

	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}

	public String getTrip_end_timestamp() {
		return trip_end_timestamp;
	}

	public int getPickup_community_area() {
		return pickup_community_area;
	}

	public int getDropoff_community_area() {
		return dropoff_community_area;
	}

	public String getPickup_centroid_latitude() {
		return pickup_centroid_latitude;
	}

	public String getPickup_centroid_longitude() {
		return pickup_centroid_longitude;
	}


	@Override
	public int compareTo(Servicio o) {
		// TODO Auto-generated method stub
		return this.getTrip_start_timestamp().compareTo(o.getTrip_start_timestamp());
	}
}
package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

import com.google.gson.JsonObject;



public class Lista<T extends Comparable<T>> implements Iterable<T>, IList<T>{

	private Node<T> head;
	private Node<T> tail;
	private int size;
	private Node<T> current = head;

	private static class Node<T> {
		private T item;
		private Node<T> next;
		private Node<T> prev;
	}

	public Lista ()
	{
		head = null;
		tail = null;
		size = 0;
	}

	public void add(T element){

		if (isEmpty())
		{
			head = new Node<T>();
			head.item = element;
			current = head;
			tail = head;
		}
		else 
		{
			Node<T> oldtail = tail;
			tail = new Node<T>();
			tail.item = element;
			oldtail.next = tail;
			tail.prev = oldtail;
		}

		size++;
	}


	public void delete(T element)
	{
		Node<T> cur = head;
		Node<T> curT = tail;

		if(cur.item.compareTo(element) == 0)
		{ 
			head = cur.next;
			head.prev = null;

		}

		while (cur.next!= null){

			cur = cur.next;

			if(cur.item.compareTo(element) == 0)
			{ 
				if(cur != head && cur != tail)
				{
					cur.prev.next = cur.next;
					cur.next.prev = cur.prev;
				}

				else if (curT.item.compareTo(element) == 0)
				{
					tail = cur.prev;
					cur.prev.next = null;
				}

				size--;
			}
		}
	}

	public T get(T element)
	{

		Node<T> cur = head;
		Node<T> curT = tail;

		if(head != null && tail != null) {

			if(cur.item.compareTo(element) == 0)
			{ 
				return (T) cur.item;
			}

			if(curT.item.compareTo(element) == 0)
			{ 
				return (T) curT.item;
			}

			else while (cur.next!= null)
			{	
				if(cur.item.compareTo(element) == 0)
				{ 
					return (T) cur.item;
				}
				cur = cur.next;
			}

			if(curT.item == null) 
			{
				return null;
			}
		}
		return null;
	}

	public boolean isEmpty ()
	{
		return size == 0;
	}

	public int getSize()
	{
		return size;
	}

	public Object getFirst()
	{
		if( isEmpty() )
		{  
			return null;
		}else
		{
			return (Object) head.item;
		}
	}

	public T next ()
	{
		if (isEmpty())
		{
			return null;
		}
		else
		{
			current = current.next;
			return (T) current.item;
		}

	}

	public T getCurrent()
	{
		return (T) current.item;
	}

	public void listing(){
		current = head;
	}


	public Iterator<T> iterator() {
		return new ListIterator<T>(head);
	}

	private class ListIterator<Item> implements Iterator<Item> {
		private Node<Item> current;

		public ListIterator(Node<Item> first) {
			current = first;
		}

		public boolean hasNext() {
			return current != null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public Item next() {
			if (!hasNext()) throw new NoSuchElementException();
			Item item = current.item;
			current = current.next; 
			return item;
		}
		
		public void addNext(Item item) {
			Node<Item> aux = null;
			current = aux;
			current.next=(Node<Item>) item;
			current.next.next = aux.next;
		}
	}
		

}



package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import API.ITaxiTripsManager;
import model.data_structures.ArbolRedBlack;
import model.data_structures.HashTableProbing;
import model.data_structures.Heap;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.data_structures.Shell;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.TaxiConServicios;
import model.vo.TaxiConServicios;

public class TaxiTripsManager implements ITaxiTripsManager {
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	

	private ArbolRedBlack arbolBal;
	private HashTableProbing tablaHashArea;
	private HashTableProbing tablaHashDura;
	private HashTableProbing tablaHashId;
	private Lista<TaxiConServicios> taxis = new Lista<TaxiConServicios>();
	private Lista<TaxiConPuntos> taxisPuntos = new Lista<TaxiConPuntos>();


	public boolean cargarSistemaLarge() {
		
		arbolBal = new ArbolRedBlack();
		tablaHashId = new HashTableProbing();
		tablaHashArea = new HashTableProbing<Integer,TaxiConServicios>();
		tablaHashDura = new HashTableProbing<Integer,TaxiConServicios>();
		Lista<Servicio> serviciosOrdenados = new Lista<Servicio>();
		Lista<TaxiConServicios>[] taxisEnCompania = new Lista[8];	
		
		cargarSistema2("./data/taxi-trips-wrvz-psew-subset-02-02-2017.json", taxisEnCompania);
		cargarSistema2("./data/taxi-trips-wrvz-psew-subset-03-02-2017.json", taxisEnCompania);
		cargarSistema2("./data/taxi-trips-wrvz-psew-subset-04-02-2017.json", taxisEnCompania);
		cargarSistema2("./data/taxi-trips-wrvz-psew-subset-05-02-2017.json", taxisEnCompania);
		cargarSistema2("./data/taxi-trips-wrvz-psew-subset-06-02-2017.json", taxisEnCompania);
		cargarSistema2("./data/taxi-trips-wrvz-psew-subset-07-02-2017.json", taxisEnCompania);
		cargarSistema2("./data/taxi-trips-wrvz-psew-subset-08-02-2017.json", taxisEnCompania);
		
		return true;
	}

	@Override
	public boolean cargarSistema(String direccionJson) {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		Lista<TaxiConServicios>[] taxisEnCompania = new Lista[8];	
		String[] companias = new String[8];
		arbolBal = new ArbolRedBlack();
		tablaHashId = new HashTableProbing();
		tablaHashArea = new HashTableProbing<Integer,TaxiConServicios>();
		tablaHashDura = new HashTableProbing<Integer,TaxiConServicios>();
		Lista<Servicio> serviciosOrdenados = new Lista<Servicio>();
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		Shell listaOrdenada = new Shell();
		int tamanoArea =0;
		int maxArea = 0;
		int tamanoDura =0;
		int tamanoDuraT =0;
		int maxDura = 0;
		companias[0]= new String("Blue Ribbon Taxi Association Inc.");
		companias[1]= new String("Taxi Affiliation Services");
		companias[2]= new String("Dispatch Taxi Affiliation");
		companias[3]= new String("Northwest Management LLC");
		companias[4]= new String("Chicago Medallion Leasing INC");
		companias[5]= new String("Choice Taxi Association");
		companias[6]= new String("Top Cab Affiliation");
		companias[7]= new String("Independent Owner");

		try {
			reader = new BufferedReader(new FileReader(direccionJson));
			Gson gson = new GsonBuilder().create();
			Servicio[] servicios = gson.fromJson(reader, Servicio[].class);
			serviciosOrdenados = listaOrdenada.sortServicios(servicios);

			for (int i = 0; i < servicios.length; i++) {
				TaxiConServicios taxi = new TaxiConServicios(servicios[i].getTaxi_id(), servicios[i].getCompany());
				TaxiConPuntos taxiP = new TaxiConPuntos(servicios[i].getTaxi_id(), servicios[i].getCompany());

				if(taxis.get(taxi)==null) {
					taxis.add(taxi);
					taxisPuntos.add(taxiP);
				}				
			}

			Iterator<TaxiConServicios> taxIte = taxis.iterator();
			Iterator<TaxiConPuntos> taxIteP = taxisPuntos.iterator();

			while(taxIte.hasNext()) {
				TaxiConServicios taxi = taxIte.next(); 
				TaxiConPuntos taxip = taxIteP.next();

				for(int i = 0; i < servicios.length; i++)
				{
					if(taxi.getTaxiId().compareTo(servicios[i].getTaxi_id()) == 0) {
						taxi.agregarServicio(servicios[i]);
						taxip.agregarServicio(servicios[i]);
					}
				}
			}

			for (int i = 0; i < taxisEnCompania.length; i++) {
				taxisEnCompania[i]=new Lista<TaxiConServicios>();
			}

			Iterator<TaxiConServicios> taxisIte = taxis.iterator();

			while (taxisIte.hasNext()) {
				TaxiConServicios taxi = taxisIte.next(); 
				if (taxi.getCompania()!=null) {
					for(int j = 0; j < companias.length; j++) {
						if (taxi.getCompania().equals(companias[j])) {
							taxisEnCompania[j].add(taxi);
						}
					}
				}

				else {taxisEnCompania[7].add(taxi);}
			}

			for(int i = 0; i < companias.length; i++) {
				System.out.println("Compania: "+companias[i]+"  Numero de taxis en compania: "+ taxisEnCompania[i].getSize());
				arbolBal.put(companias[i], taxisEnCompania[i]);
			}

			for (int i = 0; i < servicios.length; i++) {
				if(servicios[i].getPickup_community_area()>=maxArea) {
					maxArea = servicios[i].getPickup_community_area();
					tamanoArea = maxArea;
				}

				if(servicios[i].getTrip_seconds()>=maxDura) {
					maxDura = servicios[i].getTrip_seconds();
					tamanoDuraT = maxDura;
				}
			}

			if(tamanoDuraT%60!=0) {
				tamanoDura = (tamanoDuraT/60)+1;
			}
			else {
				tamanoDura = tamanoDuraT/60;
			}

			Lista<Servicio>[] serviciosPorDura = new Lista[tamanoDura];
			Lista<Servicio>[] serviciosPorArea = new Lista[tamanoArea];

			for (int i = 0; i < serviciosPorArea.length; i++) {
				serviciosPorArea[i]=new Lista<Servicio>();
			}

			for (int i = 0; i < serviciosPorDura.length; i++) {
				serviciosPorDura[i]=new Lista<Servicio>();
			}




			Iterator<Servicio> servicioIte = serviciosOrdenados.iterator();


			while(servicioIte.hasNext()) {
				Servicio servicio = servicioIte.next();

				for (int j = 0; j < serviciosPorArea.length; j++) {
					if(servicio.getPickup_community_area() == j) {
						serviciosPorArea[j].add(servicio);
					}
					for (int x = 0; x < serviciosPorDura.length; x++) {
						if(servicio.getTrip_seconds() <= (x+1)*60 && servicio.getTrip_seconds() > x*60) {
							if (serviciosPorDura[x].get(servicio) == null) {
							serviciosPorDura[x].add(servicio);		
							}
						}
					}
				}

			}

			for (int i = 0; i < serviciosPorArea.length; i++) {
				tablaHashArea.put(i, serviciosPorArea[i]);
			}

			for (int i = 0; i < serviciosPorDura.length; i++) {
				tablaHashDura.put(i*60, serviciosPorDura[i]);
			}

			Iterator<TaxiConServicios> taxiIte = taxis.iterator();

			while(taxiIte.hasNext()) {
				TaxiConServicios taxi = taxiIte.next();
				tablaHashId.put(taxi.getTaxiId(), taxi);
			}

			//Demostracion de resultados:

			//Tabla Hash por Area y Duracion

//									for (int i = 0; i < serviciosPorArea.length; i++) {
//										Iterator<Servicio> servIte = ((Lista<Servicio>) tablaHashArea.get(i)).iterator();
//										System.out.println("Area "+i+": ");
//										while(servIte.hasNext()) {
//											Servicio servicio = servIte.next();
//											System.out.println("Tiempo de inicio: "+servicio.getTrip_start_timestamp()+"   Area: "+servicio.getPickup_community_area());
//										}
//										System.out.println();
//									}
									

			return true;

		} catch (FileNotFoundException e) {
			return false;
		}
	}


	public boolean cargarSistema2(String direccionJson, Lista<TaxiConServicios>[] taxisEnCompania) {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		String[] companias = new String[8];
		Shell listaOrdenada = new Shell();
		int tamanoArea =0;
		int maxArea = 0;
		int tamanoDura =0;
		int tamanoDuraT =0;
		int maxDura = 0;
		companias[0]= new String("Blue Ribbon Taxi Association Inc.");
		companias[1]= new String("Taxi Affiliation Services");
		companias[2]= new String("Dispatch Taxi Affiliation");
		companias[3]= new String("Northwest Management LLC");
		companias[4]= new String("Chicago Medallion Leasing INC");
		companias[5]= new String("Choice Taxi Association");
		companias[6]= new String("Top Cab Affiliation");
		companias[7]= new String("Independent Owner");

		try {
			reader = new BufferedReader(new FileReader(direccionJson));
			Gson gson = new GsonBuilder().create();
			Servicio[] servicios = gson.fromJson(reader, Servicio[].class);
			Lista<Servicio> serviciosOrdenados = new Lista<Servicio>();
			serviciosOrdenados = listaOrdenada.sortServicios(servicios);
			
			for (int i = 0; i < servicios.length; i++) {
				TaxiConServicios taxi = new TaxiConServicios(servicios[i].getTaxi_id(), servicios[i].getCompany());
				TaxiConPuntos taxiP = new TaxiConPuntos(servicios[i].getTaxi_id(), servicios[i].getCompany());

				if(taxis.get(taxi)==null) {
					taxis.add(taxi);
					taxisPuntos.add(taxiP);
				}				
			}

			Iterator<TaxiConServicios> taxIte = taxis.iterator();
			Iterator<TaxiConPuntos> taxIteP = taxisPuntos.iterator();

			while(taxIte.hasNext()) {
				TaxiConServicios taxi = taxIte.next(); 
				TaxiConPuntos taxip = taxIteP.next();

				for(int i = 0; i < servicios.length; i++)
				{
					if(taxi.getTaxiId().compareTo(servicios[i].getTaxi_id()) == 0) {
						taxi.agregarServicio(servicios[i]);
						taxip.agregarServicio(servicios[i]);
					}
				}
			}

			for (int i = 0; i < taxisEnCompania.length; i++) {
				taxisEnCompania[i]=new Lista<TaxiConServicios>();
			}

			Iterator<TaxiConServicios> taxisIte = taxis.iterator();

			while (taxisIte.hasNext()) {
				TaxiConServicios taxi = taxisIte.next(); 
				if (taxi.getCompania()!=null) {
					for(int j = 0; j < companias.length; j++) {
						if (taxi.getCompania().equals(companias[j])) {
							taxisEnCompania[j].add(taxi);
						}
					}
				}

				else {taxisEnCompania[7].add(taxi);}
			}

			for(int i = 0; i < companias.length; i++) {
				System.out.println("Compania: "+companias[i]+"  Numero de taxis en compania: "+ taxisEnCompania[i].getSize());
				arbolBal.put(companias[i], taxisEnCompania[i]);
			}

			for (int i = 0; i < servicios.length; i++) {
				if(servicios[i].getPickup_community_area()>=maxArea) {
					maxArea = servicios[i].getPickup_community_area();
					tamanoArea = maxArea;
				}

				if(servicios[i].getTrip_seconds()>=maxDura) {
					maxDura = servicios[i].getTrip_seconds();
					tamanoDuraT = maxDura;
				}
			}

			if(tamanoDuraT%60!=0) {
				tamanoDura = (tamanoDuraT/60)+1;
			}
			else {
				tamanoDura = tamanoDuraT/60;
			}

			Lista<Servicio>[] serviciosPorDura = new Lista[tamanoDura];
			Lista<Servicio>[] serviciosPorArea = new Lista[tamanoArea];

			for (int i = 0; i < serviciosPorArea.length; i++) {
				serviciosPorArea[i]=new Lista<Servicio>();
			}

			for (int i = 0; i < serviciosPorDura.length; i++) {
				serviciosPorDura[i]=new Lista<Servicio>();
			}




			Iterator<Servicio> servicioIte = serviciosOrdenados.iterator();


			while(servicioIte.hasNext()) {
				Servicio servicio = servicioIte.next();

				for (int j = 0; j < serviciosPorArea.length; j++) {
					if(servicio.getPickup_community_area() == j) {
						serviciosPorArea[j].add(servicio);
					}
					for (int x = 0; x < serviciosPorDura.length; x++) {
						if(servicio.getTrip_seconds() <= (x+1)*60 && servicio.getTrip_seconds() > x*60) {
							if (serviciosPorDura[x].get(servicio) == null) {
								serviciosPorDura[x].add(servicio);		
								}					
						}

					}
				}

			}

			for (int i = 0; i < serviciosPorArea.length; i++) {
				tablaHashArea.put(i, serviciosPorArea[i]);
			}

			for (int i = 0; i < serviciosPorDura.length; i++) {
				tablaHashDura.put(i*60, serviciosPorDura[i]);
			}

			Iterator<TaxiConServicios> taxiIte = taxis.iterator();

			while(taxiIte.hasNext()) {
				TaxiConServicios taxi = taxiIte.next();
				tablaHashId.put(taxi.getTaxiId(), taxi);
			}

			//Demostracion de resultados:

			//Tabla Hash por Area y Duracion

									for (int i = 0; i < serviciosPorArea.length; i++) {
										Iterator<Servicio> servIte = ((Lista<Servicio>) tablaHashArea.get(i)).iterator();
										System.out.println("Area "+i+": ");
										while(servIte.hasNext()) {
											Servicio servicio = servIte.next();
											System.out.println("Tiempo de inicio: "+servicio.getTrip_start_timestamp()+"   Area: "+servicio.getPickup_community_area());
										}
										System.out.println();
									}
									
			System.out.println("\n");
			
			return true;
			

		} catch (FileNotFoundException e) {
			return false;
		}
	}
	@Override
	public IList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {
		// TODO Auto-generated method stub
		Lista<TaxiConServicios> respuesta = new Lista<TaxiConServicios>();
		Lista<TaxiConServicios> listaTaxis = new Lista<TaxiConServicios>();
		Lista<Servicio> listaServ = new Lista<Servicio>();
		TaxiConServicios taxiMasServicios = null;
		int max =0;

		listaTaxis = (Lista<TaxiConServicios>) arbolBal.get(compania);
		listaServ = (Lista<Servicio>) tablaHashArea.get(zonaInicio);

		Iterator<TaxiConServicios> taxiIte = listaTaxis.iterator();

		while(taxiIte.hasNext()) {
			int maxLocal =0;

			TaxiConServicios taxi = taxiIte.next();
			Lista<Servicio> servi = (Lista<Servicio>) taxi.getServicios();
			Iterator<Servicio> servIte = servi.iterator();

			while(servIte.hasNext()) {
				Servicio servicio = servIte.next();

				if(listaServ.get(servicio) != null) {
					maxLocal++;
				}
			}

			if(maxLocal > max) {
				max = maxLocal;
				taxiMasServicios = taxi;
			}

		}

		respuesta.add(taxiMasServicios);

		while(taxiIte.hasNext()) {
			int maxLocal =0;

			TaxiConServicios taxi = taxiIte.next();
			Lista<Servicio> servi = (Lista<Servicio>) taxi.getServicios();
			Iterator<Servicio> servIte = servi.iterator();
			while(servIte.hasNext()) {
				Servicio servicio = servIte.next();

				if(listaServ.get(servicio) != null) {
					maxLocal++;
				}
			}

			if(maxLocal == max) {
				respuesta.add(taxi);

			}

		}


		return respuesta;
	}


	@Override
	public IList<Servicio> A2ServiciosPorDuracion(int duracion) {
		// TODO Auto-generated method stub
		Lista<Servicio> listaServicios = new Lista<Servicio>();
		int key = 0;

		if(duracion%60!=0) {
			key = (duracion - (duracion%60));
		}
		else {
			key = duracion;
		}

		listaServicios = (Lista<Servicio>) tablaHashDura.get(key);

		return listaServicios;
	}


	@Override
	public IList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public IList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		Heap listaOrdenada = new Heap();
		TaxiConPuntos[] taxisAOrdenar = new TaxiConPuntos[taxis.getSize()];
		Iterator<TaxiConPuntos> taxisIte = taxisPuntos.iterator();
		int i =0;


		while(taxisIte.hasNext()) {
			TaxiConPuntos taxi = taxisIte.next();
			taxisAOrdenar[i] = taxi;
			i++;
		}		

		taxisPuntos = listaOrdenada.sortTaxis(taxisAOrdenar);

		Iterator<TaxiConPuntos> taxisOrdenados = taxisPuntos.iterator();

		while(taxisOrdenados.hasNext()) {
			TaxiConPuntos taxi = taxisOrdenados.next();
			System.out.println("Id del taxi: "+taxi.getTaxiId()+" Puntos: "+taxi.getPuntos());
		}

		return null;
	}

	@Override
	public IList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C, double latitudReq2C, double longitudReq2C) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}

	@Override
	public IList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}
}
